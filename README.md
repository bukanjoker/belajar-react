## Requirement Before Start the Project

- This project is using **Node.js** click here for [download](https://nodejs.org/en/download/), if you're already have just ignore this
- NPM version > **6.10**
```
$ npm -v
```
- Node version > **12.0**
```
$ node -v
```
- Check this [forum](https://stackoverflow.com/a/31520672) to get how to update **Node.js**, if your node & npm version has fulfilled just ignore this

## Start The Project

- clone this project
```
$ git clone https://gitlab.com/bukanjoker/belajar-react.git
```
- checkout to branch `development`
```
$ git checkout development
```
- check if there're any dependency added to project (not always)
```
$ npm install
```
- start the project
```
$ npm start
```

### Happy Coding !
